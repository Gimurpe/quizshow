var express = require('express')
var fs = require('fs')
var app = express();

var quiz = JSON.parse(fs.readFileSync('quiz.json'));


app.set('view engine', 'html');

app.set('port', (process.env.PORT || 8080)) /* used to be 5000 */
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(request, response) {
buffer = new Buffer(fs.readFileSync('public/index.html'))
response.send(buffer.toString('utf8'))
})

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});
/*app.listen(app.get('port'), function() {
console.log("Node app is running at localhost:" + app.get('port'))
})


/*var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();

app.set('port', process.env.PORT || 3000);

app.engine('.html', require('ejs').__express);
app.set('views', path.join(__dirname, 'public/views'));
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}


app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


require('./app/routes')(app);

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

module.exports = app;
